
package net.mcreator.pokeymod.enchantment;

import net.minecraft.world.item.enchantment.EnchantmentCategory;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.entity.EquipmentSlot;

import net.mcreator.pokeymod.init.PokeyModModItems;

public class TungstenlikestrengthEnchantment extends Enchantment {
	public TungstenlikestrengthEnchantment(EquipmentSlot... slots) {
		super(Enchantment.Rarity.COMMON, EnchantmentCategory.WEARABLE, slots);
	}

	@Override
	public int getMaxLevel() {
		return 18;
	}

	@Override
	public boolean canApplyAtEnchantingTable(ItemStack stack) {
		if (stack.getItem() == PokeyModModItems.TUNGSTEN_AROMOR_HELMET)
			return true;
		if (stack.getItem() == PokeyModModItems.TUNGSTEN_AROMOR_CHESTPLATE)
			return true;
		if (stack.getItem() == PokeyModModItems.TUNGSTEN_AROMOR_LEGGINGS)
			return true;
		if (stack.getItem() == PokeyModModItems.TUNGSTEN_AROMOR_BOOTS)
			return true;
		if (stack.getItem() == PokeyModModItems.TUNGSTEN_PICKAXE)
			return true;
		if (stack.getItem() == PokeyModModItems.TUNGSTEN_AXE)
			return true;
		if (stack.getItem() == PokeyModModItems.RENFORCED_ARMOR_HELMET)
			return true;
		if (stack.getItem() == PokeyModModItems.RENFORCED_ARMOR_CHESTPLATE)
			return true;
		if (stack.getItem() == PokeyModModItems.RENFORCED_ARMOR_LEGGINGS)
			return true;
		if (stack.getItem() == PokeyModModItems.RENFORCED_ARMOR_BOOTS)
			return true;
		if (stack.getItem() == PokeyModModItems.RENFORCED_TUNGSTEN_PICKAXE)
			return true;
		if (stack.getItem() == PokeyModModItems.TUNGSTEN_SWORD)
			return true;
		if (stack.getItem() == PokeyModModItems.TUNGSTEN_SHOVEL)
			return true;
		if (stack.getItem() == PokeyModModItems.TUNGSTEN_HOE)
			return true;
		if (stack.getItem() == PokeyModModItems.RENFORCED_TUNGSTEN_AXE)
			return true;
		if (stack.getItem() == PokeyModModItems.RENFORCED_TUNGSTEN_SWORD)
			return true;
		if (stack.getItem() == PokeyModModItems.RENFORCED_TUNGSTEN_SHOVEL)
			return true;
		if (stack.getItem() == PokeyModModItems.RENFORCED_TUNGSTEN_HOE)
			return true;
		if (stack.getItem() == Items.WOODEN_SWORD)
			return true;
		if (stack.getItem() == Items.WOODEN_SHOVEL)
			return true;
		if (stack.getItem() == Items.WOODEN_PICKAXE)
			return true;
		if (stack.getItem() == Items.WOODEN_AXE)
			return true;
		if (stack.getItem() == Items.WOODEN_HOE)
			return true;
		if (stack.getItem() == Items.STONE_SWORD)
			return true;
		if (stack.getItem() == Items.STONE_SHOVEL)
			return true;
		if (stack.getItem() == Items.STONE_PICKAXE)
			return true;
		if (stack.getItem() == Items.STONE_AXE)
			return true;
		if (stack.getItem() == Items.STONE_HOE)
			return true;
		if (stack.getItem() == Items.IRON_SWORD)
			return true;
		if (stack.getItem() == Items.IRON_SHOVEL)
			return true;
		if (stack.getItem() == Items.IRON_PICKAXE)
			return true;
		if (stack.getItem() == Items.IRON_AXE)
			return true;
		if (stack.getItem() == Items.IRON_HOE)
			return true;
		if (stack.getItem() == Items.FLINT_AND_STEEL)
			return true;
		if (stack.getItem() == Items.SHEARS)
			return true;
		if (stack.getItem() == Items.GOLDEN_SWORD)
			return true;
		if (stack.getItem() == Items.GOLDEN_SHOVEL)
			return true;
		if (stack.getItem() == Items.GOLDEN_PICKAXE)
			return true;
		if (stack.getItem() == Items.GOLDEN_AXE)
			return true;
		if (stack.getItem() == Items.GOLDEN_HOE)
			return true;
		if (stack.getItem() == Items.DIAMOND_SWORD)
			return true;
		if (stack.getItem() == Items.DIAMOND_SHOVEL)
			return true;
		if (stack.getItem() == Items.DIAMOND_PICKAXE)
			return true;
		if (stack.getItem() == Items.DIAMOND_AXE)
			return true;
		if (stack.getItem() == Items.DIAMOND_HOE)
			return true;
		if (stack.getItem() == Items.NETHERITE_SWORD)
			return true;
		if (stack.getItem() == Items.NETHERITE_SHOVEL)
			return true;
		if (stack.getItem() == Items.NETHERITE_PICKAXE)
			return true;
		if (stack.getItem() == Items.NETHERITE_AXE)
			return true;
		if (stack.getItem() == Items.NETHERITE_HOE)
			return true;
		if (stack.getItem() == Items.TRIDENT)
			return true;
		if (stack.getItem() == Items.LEATHER_HELMET)
			return true;
		if (stack.getItem() == Items.LEATHER_CHESTPLATE)
			return true;
		if (stack.getItem() == Items.LEATHER_LEGGINGS)
			return true;
		if (stack.getItem() == Items.LEATHER_BOOTS)
			return true;
		if (stack.getItem() == Items.CHAINMAIL_HELMET)
			return true;
		if (stack.getItem() == Items.CHAINMAIL_CHESTPLATE)
			return true;
		if (stack.getItem() == Items.CHAINMAIL_LEGGINGS)
			return true;
		if (stack.getItem() == Items.CHAINMAIL_BOOTS)
			return true;
		if (stack.getItem() == Items.TURTLE_HELMET)
			return true;
		if (stack.getItem() == Items.IRON_HELMET)
			return true;
		if (stack.getItem() == Items.IRON_CHESTPLATE)
			return true;
		if (stack.getItem() == Items.IRON_LEGGINGS)
			return true;
		if (stack.getItem() == Items.IRON_BOOTS)
			return true;
		if (stack.getItem() == Items.GOLDEN_HELMET)
			return true;
		if (stack.getItem() == Items.GOLDEN_CHESTPLATE)
			return true;
		if (stack.getItem() == Items.GOLDEN_LEGGINGS)
			return true;
		if (stack.getItem() == Items.GOLDEN_BOOTS)
			return true;
		if (stack.getItem() == Items.DIAMOND_HELMET)
			return true;
		if (stack.getItem() == Items.DIAMOND_CHESTPLATE)
			return true;
		if (stack.getItem() == Items.DIAMOND_LEGGINGS)
			return true;
		if (stack.getItem() == Items.DIAMOND_BOOTS)
			return true;
		if (stack.getItem() == Items.NETHERITE_HELMET)
			return true;
		if (stack.getItem() == Items.NETHERITE_CHESTPLATE)
			return true;
		if (stack.getItem() == Items.NETHERITE_LEGGINGS)
			return true;
		if (stack.getItem() == Items.NETHERITE_BOOTS)
			return true;
		if (stack.getItem() == Items.ELYTRA)
			return true;
		if (stack.getItem() == Items.LEATHER_HORSE_ARMOR)
			return true;
		if (stack.getItem() == Items.IRON_HORSE_ARMOR)
			return true;
		if (stack.getItem() == Items.GOLDEN_HORSE_ARMOR)
			return true;
		if (stack.getItem() == Items.DIAMOND_HORSE_ARMOR)
			return true;
		return false;
	}
}
