
package net.mcreator.pokeymod.enchantment;

import net.minecraft.world.item.enchantment.EnchantmentCategory;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.damagesource.DamageSource;

public class MasterstoolEnchantment extends Enchantment {
	public MasterstoolEnchantment(EquipmentSlot... slots) {
		super(Enchantment.Rarity.VERY_RARE, EnchantmentCategory.WEAPON, slots);
	}

	@Override
	public int getMaxLevel() {
		return 8;
	}

	@Override
	public int getDamageProtection(int level, DamageSource source) {
		return level * 12;
	}
}
