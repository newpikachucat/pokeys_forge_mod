
package net.mcreator.pokeymod.item;

import net.mcreator.pokeymod.init.PokeyModModTabs;

public class Renforced_tungstenIngotItem extends Item {
	public Renforced_tungstenIngotItem() {
		super(new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB).stacksTo(64).fireResistant().rarity(Rarity.EPIC));
		setRegistryName("renforced_tungsten_ingot");
	}

	@Override
	public int getEnchantmentValue() {
		return 98;
	}

	@Override
	public int getUseDuration(ItemStack itemstack) {
		return 0;
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public boolean isFoil(ItemStack itemstack) {
		return true;
	}
}
