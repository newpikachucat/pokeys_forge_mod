
package net.mcreator.pokeymod.item;

import net.mcreator.pokeymod.init.PokeyModModTabs;

import java.util.List;

public class TungstenIngotItem extends Item {
	public TungstenIngotItem() {
		super(new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB).stacksTo(64).fireResistant().rarity(Rarity.RARE));
		setRegistryName("tungsten_ingot");
	}

	@Override
	public int getEnchantmentValue() {
		return 400;
	}

	@Override
	public int getUseDuration(ItemStack itemstack) {
		return 0;
	}

	@Override
	public float getDestroySpeed(ItemStack par1ItemStack, BlockState par2Block) {
		return 3F;
	}

	@Override
	public void appendHoverText(ItemStack itemstack, Level world, List<Component> list, TooltipFlag flag) {
		super.appendHoverText(itemstack, world, list, flag);
		list.add(new TextComponent("tungsten"));
		list.add(new TextComponent("medel"));
	}
}
