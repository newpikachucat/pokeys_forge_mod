
package net.mcreator.pokeymod.item;

import net.mcreator.pokeymod.init.PokeyModModTabs;
import net.mcreator.pokeymod.init.PokeyModModSounds;

public class PlaceholdermusicdiscItem extends RecordItem {
	public PlaceholdermusicdiscItem() {
		super(0, PokeyModModSounds.REGISTRY.get(new ResourceLocation("pokey_mod:sound_placeholder_name")),
				new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB).stacksTo(1).rarity(Rarity.RARE));
		setRegistryName("placeholdermusicdisc");
	}
}
