
package net.mcreator.pokeymod.item;

import net.minecraft.sounds.SoundEvent;

import net.mcreator.pokeymod.init.PokeyModModTabs;

public abstract class PotatoArmorItem extends ArmorItem {
	public PotatoArmorItem(EquipmentSlot slot, Item.Properties properties) {
		super(new ArmorMaterial() {
			@Override
			public int getDurabilityForSlot(EquipmentSlot slot) {
				return new int[]{13, 15, 16, 11}[slot.getIndex()] * 11;
			}

			@Override
			public int getDefenseForSlot(EquipmentSlot slot) {
				return new int[]{2, 5, 4, 2}[slot.getIndex()];
			}

			@Override
			public int getEnchantmentValue() {
				return 7;
			}

			@Override
			public SoundEvent getEquipSound() {
				return ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation("pokey_mod:potato_armor_equip"));
			}

			@Override
			public Ingredient getRepairIngredient() {
				return Ingredient.of(new ItemStack(Items.POTATO));
			}

			@Override
			public String getName() {
				return "potato_armor";
			}

			@Override
			public float getToughness() {
				return 0.5f;
			}

			@Override
			public float getKnockbackResistance() {
				return 0.05f;
			}
		}, slot, properties);
	}

	public static class Helmet extends PotatoArmorItem {
		public Helmet() {
			super(EquipmentSlot.HEAD, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB));
			setRegistryName("potato_armor_helmet");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/potato_layer_1.png";
		}
	}

	public static class Chestplate extends PotatoArmorItem {
		public Chestplate() {
			super(EquipmentSlot.CHEST, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB));
			setRegistryName("potato_armor_chestplate");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/potato_layer_1.png";
		}
	}

	public static class Leggings extends PotatoArmorItem {
		public Leggings() {
			super(EquipmentSlot.LEGS, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB));
			setRegistryName("potato_armor_leggings");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/potato_layer_2.png";
		}
	}

	public static class Boots extends PotatoArmorItem {
		public Boots() {
			super(EquipmentSlot.FEET, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB));
			setRegistryName("potato_armor_boots");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/potato_layer_1.png";
		}
	}
}
