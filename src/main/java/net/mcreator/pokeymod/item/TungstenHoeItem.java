
package net.mcreator.pokeymod.item;

import net.mcreator.pokeymod.init.PokeyModModTabs;
import net.mcreator.pokeymod.init.PokeyModModItems;

public class TungstenHoeItem extends HoeItem {
	public TungstenHoeItem() {
		super(new Tier() {
			public int getUses() {
				return 6280;
			}

			public float getSpeed() {
				return 24f;
			}

			public float getAttackDamageBonus() {
				return 18f;
			}

			public int getLevel() {
				return 20;
			}

			public int getEnchantmentValue() {
				return 140;
			}

			public Ingredient getRepairIngredient() {
				return Ingredient.of(new ItemStack(PokeyModModItems.TUNGSTEN_INGOT));
			}
		}, 0, -3f, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB));
		setRegistryName("tungsten_hoe");
	}
}
