
package net.mcreator.pokeymod.item;

import net.minecraft.sounds.SoundEvent;

import net.mcreator.pokeymod.init.PokeyModModTabs;

public abstract class Leafy_armorArmorItem extends ArmorItem {
	public Leafy_armorArmorItem(EquipmentSlot slot, Item.Properties properties) {
		super(new ArmorMaterial() {
			@Override
			public int getDurabilityForSlot(EquipmentSlot slot) {
				return new int[]{13, 15, 16, 11}[slot.getIndex()] * 100;
			}

			@Override
			public int getDefenseForSlot(EquipmentSlot slot) {
				return new int[]{1, 1, 1, 1}[slot.getIndex()];
			}

			@Override
			public int getEnchantmentValue() {
				return 2;
			}

			@Override
			public SoundEvent getEquipSound() {
				return ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation(""));
			}

			@Override
			public Ingredient getRepairIngredient() {
				return Ingredient.of(new ItemStack(Blocks.OAK_LEAVES));
			}

			@Override
			public String getName() {
				return "leafy_armor_armor";
			}

			@Override
			public float getToughness() {
				return 0.125f;
			}

			@Override
			public float getKnockbackResistance() {
				return 0.5f;
			}
		}, slot, properties);
	}

	public static class Helmet extends Leafy_armorArmorItem {
		public Helmet() {
			super(EquipmentSlot.HEAD, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB));
			setRegistryName("leafy_armor_armor_helmet");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/leafy_armor_layer_1.png";
		}
	}

	public static class Chestplate extends Leafy_armorArmorItem {
		public Chestplate() {
			super(EquipmentSlot.CHEST, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB));
			setRegistryName("leafy_armor_armor_chestplate");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/leafy_armor_layer_1.png";
		}
	}

	public static class Leggings extends Leafy_armorArmorItem {
		public Leggings() {
			super(EquipmentSlot.LEGS, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB));
			setRegistryName("leafy_armor_armor_leggings");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/leafy_armor_layer_2.png";
		}
	}

	public static class Boots extends Leafy_armorArmorItem {
		public Boots() {
			super(EquipmentSlot.FEET, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB));
			setRegistryName("leafy_armor_armor_boots");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/leafy_armor_layer_1.png";
		}
	}
}
