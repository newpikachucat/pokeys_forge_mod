
package net.mcreator.pokeymod.item;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;

public class ZeezarPortalIgniterItem extends Item {
	public ZeezarPortalIgniterItem() {
		super(new Item.Properties().tab(CreativeModeTab.TAB_MISC).stacksTo(64).rarity(Rarity.COMMON));
		setRegistryName("zeezar_portal_igniter");
	}

	@Override
	public int getUseDuration(ItemStack itemstack) {
		return 0;
	}
}
