
package net.mcreator.pokeymod.item;

import net.minecraft.sounds.SoundEvent;

import net.mcreator.pokeymod.init.PokeyModModTabs;
import net.mcreator.pokeymod.init.PokeyModModItems;

public abstract class Weighted_Renforced_armorArmorItem extends ArmorItem {
	public Weighted_Renforced_armorArmorItem(EquipmentSlot slot, Item.Properties properties) {
		super(new ArmorMaterial() {
			@Override
			public int getDurabilityForSlot(EquipmentSlot slot) {
				return new int[]{13, 15, 16, 11}[slot.getIndex()] * 50;
			}

			@Override
			public int getDefenseForSlot(EquipmentSlot slot) {
				return new int[]{18, 44, 43, 20}[slot.getIndex()];
			}

			@Override
			public int getEnchantmentValue() {
				return 90;
			}

			@Override
			public SoundEvent getEquipSound() {
				return ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation(""));
			}

			@Override
			public Ingredient getRepairIngredient() {
				return Ingredient.of(new ItemStack(PokeyModModItems.RENFORCED_TUNGSTEN_INGOT));
			}

			@Override
			public String getName() {
				return "weighted_renforced_armor_armor";
			}

			@Override
			public float getToughness() {
				return 5f;
			}

			@Override
			public float getKnockbackResistance() {
				return 1.5f;
			}
		}, slot, properties);
	}

	public static class Helmet extends Weighted_Renforced_armorArmorItem {
		public Helmet() {
			super(EquipmentSlot.HEAD, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB));
			setRegistryName("weighted_renforced_armor_armor_helmet");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/weighted_renforced_armor_layer_1.png";
		}
	}

	public static class Chestplate extends Weighted_Renforced_armorArmorItem {
		public Chestplate() {
			super(EquipmentSlot.CHEST, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB));
			setRegistryName("weighted_renforced_armor_armor_chestplate");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/weighted_renforced_armor_layer_1.png";
		}
	}

	public static class Leggings extends Weighted_Renforced_armorArmorItem {
		public Leggings() {
			super(EquipmentSlot.LEGS, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB));
			setRegistryName("weighted_renforced_armor_armor_leggings");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/weighted_renforced_armor_layer_2.png";
		}
	}

	public static class Boots extends Weighted_Renforced_armorArmorItem {
		public Boots() {
			super(EquipmentSlot.FEET, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB));
			setRegistryName("weighted_renforced_armor_armor_boots");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/weighted_renforced_armor_layer_1.png";
		}
	}
}
