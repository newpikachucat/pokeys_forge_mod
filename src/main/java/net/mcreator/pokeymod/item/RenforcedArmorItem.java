
package net.mcreator.pokeymod.item;

import net.minecraft.sounds.SoundEvent;

import net.mcreator.pokeymod.init.PokeyModModTabs;
import net.mcreator.pokeymod.init.PokeyModModItems;

public abstract class RenforcedArmorItem extends ArmorItem {
	public RenforcedArmorItem(EquipmentSlot slot, Item.Properties properties) {
		super(new ArmorMaterial() {
			@Override
			public int getDurabilityForSlot(EquipmentSlot slot) {
				return new int[]{13, 15, 16, 11}[slot.getIndex()] * 150;
			}

			@Override
			public int getDefenseForSlot(EquipmentSlot slot) {
				return new int[]{18, 44, 43, 20}[slot.getIndex()];
			}

			@Override
			public int getEnchantmentValue() {
				return 90;
			}

			@Override
			public SoundEvent getEquipSound() {
				return ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation(""));
			}

			@Override
			public Ingredient getRepairIngredient() {
				return Ingredient.of(new ItemStack(PokeyModModItems.RENFORCED_TUNGSTEN_INGOT));
			}

			@Override
			public String getName() {
				return "renforced_armor";
			}

			@Override
			public float getToughness() {
				return 4f;
			}

			@Override
			public float getKnockbackResistance() {
				return 0.9f;
			}
		}, slot, properties);
	}

	public static class Helmet extends RenforcedArmorItem {
		public Helmet() {
			super(EquipmentSlot.HEAD, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB).fireResistant());
			setRegistryName("renforced_armor_helmet");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/renforced_layer_1.png";
		}
	}

	public static class Chestplate extends RenforcedArmorItem {
		public Chestplate() {
			super(EquipmentSlot.CHEST, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB).fireResistant());
			setRegistryName("renforced_armor_chestplate");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/renforced_layer_1.png";
		}
	}

	public static class Leggings extends RenforcedArmorItem {
		public Leggings() {
			super(EquipmentSlot.LEGS, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB).fireResistant());
			setRegistryName("renforced_armor_leggings");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/renforced_layer_2.png";
		}
	}

	public static class Boots extends RenforcedArmorItem {
		public Boots() {
			super(EquipmentSlot.FEET, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB).fireResistant());
			setRegistryName("renforced_armor_boots");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/renforced_layer_1.png";
		}
	}
}
