
package net.mcreator.pokeymod.item;

import net.mcreator.pokeymod.init.PokeyModModTabs;
import net.mcreator.pokeymod.init.PokeyModModItems;

public class TungstenSwordItem extends SwordItem {
	public TungstenSwordItem() {
		super(new Tier() {
			public int getUses() {
				return 6280;
			}

			public float getSpeed() {
				return 150f;
			}

			public float getAttackDamageBonus() {
				return 66.7f;
			}

			public int getLevel() {
				return 1090;
			}

			public int getEnchantmentValue() {
				return 1000;
			}

			public Ingredient getRepairIngredient() {
				return Ingredient.of(new ItemStack(PokeyModModItems.TUNGSTEN_INGOT));
			}
		}, 3, 14.799999999999997f, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB).fireResistant());
		setRegistryName("tungsten_sword");
	}
}
