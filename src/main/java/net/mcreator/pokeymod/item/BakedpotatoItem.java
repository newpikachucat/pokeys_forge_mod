
package net.mcreator.pokeymod.item;

import net.minecraft.sounds.SoundEvent;

import net.mcreator.pokeymod.init.PokeyModModTabs;

public abstract class BakedpotatoItem extends ArmorItem {
	public BakedpotatoItem(EquipmentSlot slot, Item.Properties properties) {
		super(new ArmorMaterial() {
			@Override
			public int getDurabilityForSlot(EquipmentSlot slot) {
				return new int[]{13, 15, 16, 11}[slot.getIndex()] * 20;
			}

			@Override
			public int getDefenseForSlot(EquipmentSlot slot) {
				return new int[]{0, 1, 1, 0}[slot.getIndex()];
			}

			@Override
			public int getEnchantmentValue() {
				return 1;
			}

			@Override
			public SoundEvent getEquipSound() {
				return ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation("entity.magma_cube.squish"));
			}

			@Override
			public Ingredient getRepairIngredient() {
				return Ingredient.of(new ItemStack(Items.BAKED_POTATO));
			}

			@Override
			public String getName() {
				return "bakedpotato";
			}

			@Override
			public float getToughness() {
				return 0.06f;
			}

			@Override
			public float getKnockbackResistance() {
				return 0.01f;
			}
		}, slot, properties);
	}

	public static class Helmet extends BakedpotatoItem {
		public Helmet() {
			super(EquipmentSlot.HEAD, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB));
			setRegistryName("bakedpotato_helmet");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/potato_armor_layer_1.png";
		}
	}

	public static class Chestplate extends BakedpotatoItem {
		public Chestplate() {
			super(EquipmentSlot.CHEST, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB));
			setRegistryName("bakedpotato_chestplate");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/potato_armor_layer_1.png";
		}
	}

	public static class Leggings extends BakedpotatoItem {
		public Leggings() {
			super(EquipmentSlot.LEGS, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB));
			setRegistryName("bakedpotato_leggings");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/potato_armor_layer_2.png";
		}
	}

	public static class Boots extends BakedpotatoItem {
		public Boots() {
			super(EquipmentSlot.FEET, new Item.Properties().tab(PokeyModModTabs.TAB_POKEYSMODTAB));
			setRegistryName("bakedpotato_boots");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "pokey_mod:textures/models/armor/potato_armor_layer_1.png";
		}
	}
}
