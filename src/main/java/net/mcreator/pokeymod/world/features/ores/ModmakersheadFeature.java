
package net.mcreator.pokeymod.world.features.ores;

import net.mcreator.pokeymod.init.PokeyModModBlocks;

import java.util.Set;
import java.util.Random;

public class ModmakersheadFeature extends OreFeature {
	public static final ModmakersheadFeature FEATURE = (ModmakersheadFeature) new ModmakersheadFeature().setRegistryName("pokey_mod:modmakershead");
	public static final ConfiguredFeature<?, ?> CONFIGURED_FEATURE = FEATURE
			.configured(new OreConfiguration(ModmakersheadFeatureRuleTest.INSTANCE, PokeyModModBlocks.MODMAKERSHEAD.defaultBlockState(), 16))
			.range(new RangeDecoratorConfiguration(UniformHeight.of(VerticalAnchor.absolute(0), VerticalAnchor.absolute(81)))).squared().count(3);
	public static final Set<ResourceLocation> GENERATE_BIOMES = null;

	public ModmakersheadFeature() {
		super(OreConfiguration.CODEC);
	}

	public boolean place(FeaturePlaceContext<OreConfiguration> context) {
		WorldGenLevel world = context.level();
		ResourceKey<Level> dimensionType = world.getLevel().dimension();
		boolean dimensionCriteria = false;
		if (dimensionType == ResourceKey.create(Registry.DIMENSION_REGISTRY, new ResourceLocation("pokey_mod:zeezar")))
			dimensionCriteria = true;
		if (!dimensionCriteria)
			return false;
		return super.place(context);
	}

	private static class ModmakersheadFeatureRuleTest extends RuleTest {
		static final ModmakersheadFeatureRuleTest INSTANCE = new ModmakersheadFeatureRuleTest();
		static final com.mojang.serialization.Codec<ModmakersheadFeatureRuleTest> codec = com.mojang.serialization.Codec.unit(() -> INSTANCE);
		static final RuleTestType<ModmakersheadFeatureRuleTest> CUSTOM_MATCH = Registry.register(Registry.RULE_TEST,
				new ResourceLocation("pokey_mod:modmakershead_match"), () -> codec);

		public boolean test(BlockState blockAt, Random random) {
			boolean blockCriteria = false;
			return blockCriteria;
		}

		protected RuleTestType<?> getType() {
			return CUSTOM_MATCH;
		}
	}
}
