
package net.mcreator.pokeymod.world.features.ores;

import net.mcreator.pokeymod.init.PokeyModModBlocks;

import java.util.Set;
import java.util.Random;

public class TungstenOreFeature extends OreFeature {
	public static final TungstenOreFeature FEATURE = (TungstenOreFeature) new TungstenOreFeature().setRegistryName("pokey_mod:tungsten_ore");
	public static final ConfiguredFeature<?, ?> CONFIGURED_FEATURE = FEATURE
			.configured(new OreConfiguration(TungstenOreFeatureRuleTest.INSTANCE, PokeyModModBlocks.TUNGSTEN_ORE.defaultBlockState(), 1))
			.range(new RangeDecoratorConfiguration(UniformHeight.of(VerticalAnchor.absolute(1), VerticalAnchor.absolute(16)))).squared().count(2);
	public static final Set<ResourceLocation> GENERATE_BIOMES = null;

	public TungstenOreFeature() {
		super(OreConfiguration.CODEC);
	}

	public boolean place(FeaturePlaceContext<OreConfiguration> context) {
		WorldGenLevel world = context.level();
		ResourceKey<Level> dimensionType = world.getLevel().dimension();
		boolean dimensionCriteria = false;
		if (dimensionType == Level.OVERWORLD)
			dimensionCriteria = true;
		if (!dimensionCriteria)
			return false;
		return super.place(context);
	}

	private static class TungstenOreFeatureRuleTest extends RuleTest {
		static final TungstenOreFeatureRuleTest INSTANCE = new TungstenOreFeatureRuleTest();
		static final com.mojang.serialization.Codec<TungstenOreFeatureRuleTest> codec = com.mojang.serialization.Codec.unit(() -> INSTANCE);
		static final RuleTestType<TungstenOreFeatureRuleTest> CUSTOM_MATCH = Registry.register(Registry.RULE_TEST,
				new ResourceLocation("pokey_mod:tungsten_ore_match"), () -> codec);

		public boolean test(BlockState blockAt, Random random) {
			boolean blockCriteria = false;
			if (blockAt.getBlock() == Blocks.STONE)
				blockCriteria = true;
			return blockCriteria;
		}

		protected RuleTestType<?> getType() {
			return CUSTOM_MATCH;
		}
	}
}
