
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.pokeymod.init;

import net.minecraft.sounds.SoundEvent;

import java.util.Map;
import java.util.HashMap;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class PokeyModModSounds {
	public static Map<ResourceLocation, SoundEvent> REGISTRY = new HashMap<>();
	static {
		REGISTRY.put(new ResourceLocation("pokey_mod", "the_forest_unsettle"),
				new SoundEvent(new ResourceLocation("pokey_mod", "the_forest_unsettle")));
		REGISTRY.put(new ResourceLocation("pokey_mod", "sound_placeholder_name"),
				new SoundEvent(new ResourceLocation("pokey_mod", "sound_placeholder_name")));
		REGISTRY.put(new ResourceLocation("pokey_mod", "hype_train_of_hell_to_hell"),
				new SoundEvent(new ResourceLocation("pokey_mod", "hype_train_of_hell_to_hell")));
		REGISTRY.put(new ResourceLocation("pokey_mod", "potato_armor_equip"),
				new SoundEvent(new ResourceLocation("pokey_mod", "potato_armor_equip")));
	}

	@SubscribeEvent
	public static void registerSounds(RegistryEvent.Register<SoundEvent> event) {
		for (Map.Entry<ResourceLocation, SoundEvent> sound : REGISTRY.entrySet())
			event.getRegistry().register(sound.getValue().setRegistryName(sound.getKey()));
	}
}
