
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.pokeymod.init;

import net.mcreator.pokeymod.PokeyModMod;

import java.util.List;
import java.util.ArrayList;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class PokeyModModBiomes {
	private static final List<Biome> REGISTRY = new ArrayList<>();
	public static Biome ZEETHERIRON = register("zeetheriron", ZeetherironBiome.createBiome());
	public static Biome ZEETHERWOOD = register("zeetherwood", ZeetherwoodBiome.createBiome());

	private static Biome register(String registryname, Biome biome) {
		REGISTRY.add(biome.setRegistryName(new ResourceLocation(PokeyModMod.MODID, registryname)));
		return biome;
	}

	@SubscribeEvent
	public static void registerBiomes(RegistryEvent.Register<Biome> event) {
		event.getRegistry().registerAll(REGISTRY.toArray(new Biome[0]));
	}

	@SubscribeEvent
	public static void init(FMLCommonSetupEvent event) {
		event.enqueueWork(() -> {
			ZeetherironBiome.init();
			ZeetherwoodBiome.init();
		});
	}
}
