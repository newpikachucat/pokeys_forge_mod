
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.pokeymod.init;

import net.mcreator.pokeymod.enchantment.TungstenlikestrengthEnchantment;
import net.mcreator.pokeymod.enchantment.MasterstoolEnchantment;

import java.util.List;
import java.util.ArrayList;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class PokeyModModEnchantments {
	private static final List<Enchantment> REGISTRY = new ArrayList<>();
	public static final Enchantment MASTERSTOOL = register("pokey_mod:masterstool", new MasterstoolEnchantment());
	public static final Enchantment TUNGSTENLIKESTRENGTH = register("pokey_mod:tungstenlikestrength", new TungstenlikestrengthEnchantment());

	private static Enchantment register(String registryname, Enchantment enchantment) {
		REGISTRY.add(enchantment.setRegistryName(registryname));
		return enchantment;
	}

	@SubscribeEvent
	public static void registerEnchantments(RegistryEvent.Register<Enchantment> event) {
		event.getRegistry().registerAll(REGISTRY.toArray(new Enchantment[0]));
	}
}
