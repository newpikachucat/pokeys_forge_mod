
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.pokeymod.init;

public class PokeyModModTabs {
	public static CreativeModeTab TAB_POKEYSMODTAB;

	public static void load() {
		TAB_POKEYSMODTAB = new CreativeModeTab("tabpokeysmodtab") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(PokeyModModItems.RENFORCED_TUNGSTEN_AXE);
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return true;
			}
		}.setBackgroundSuffix("item_search.png");
	}
}
