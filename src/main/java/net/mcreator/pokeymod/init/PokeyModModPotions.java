
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.pokeymod.init;

import java.util.List;
import java.util.ArrayList;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class PokeyModModPotions {
	private static final List<Potion> REGISTRY = new ArrayList<>();
	public static final Potion STEPPYPOP = register(new Potion(new MobEffectInstance(MobEffects.MOVEMENT_SPEED, 10000, 10, false, false),
			new MobEffectInstance(MobEffects.DIG_SPEED, 10000, 10, false, false),
			new MobEffectInstance(MobEffects.DAMAGE_BOOST, 10000, 12, false, false), new MobEffectInstance(MobEffects.JUMP, 9500, 5, false, false),
			new MobEffectInstance(MobEffects.HEALTH_BOOST, 10000, 0, false, false)).setRegistryName("steppypop"));

	private static Potion register(Potion potion) {
		REGISTRY.add(potion);
		return potion;
	}

	@SubscribeEvent
	public static void registerPotions(RegistryEvent.Register<Potion> event) {
		event.getRegistry().registerAll(REGISTRY.toArray(new Potion[0]));
	}
}
