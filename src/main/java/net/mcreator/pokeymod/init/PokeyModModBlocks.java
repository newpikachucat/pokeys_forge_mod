
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.pokeymod.init;

import net.mcreator.pokeymod.block.ZeezarPortalBlock;
import net.mcreator.pokeymod.block.TungstenOreBlock;
import net.mcreator.pokeymod.block.TungstenBlockBlock;
import net.mcreator.pokeymod.block.Renforced_tungstenBlockBlock;
import net.mcreator.pokeymod.block.ModmakersheadBlock;

import java.util.List;
import java.util.ArrayList;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class PokeyModModBlocks {
	private static final List<Block> REGISTRY = new ArrayList<>();
	public static final Block ZEEZAR_PORTAL = register(new ZeezarPortalBlock());
	public static final Block TUNGSTEN_ORE = register(new TungstenOreBlock());
	public static final Block TUNGSTEN_BLOCK = register(new TungstenBlockBlock());
	public static final Block RENFORCED_TUNGSTEN_BLOCK = register(new Renforced_tungstenBlockBlock());
	public static final Block MODMAKERSHEAD = register(new ModmakersheadBlock());

	private static Block register(Block block) {
		REGISTRY.add(block);
		return block;
	}

	@SubscribeEvent
	public static void registerBlocks(RegistryEvent.Register<Block> event) {
		event.getRegistry().registerAll(REGISTRY.toArray(new Block[0]));
	}

	@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
	public static class ClientSideHandler {
		@SubscribeEvent
		public static void clientSetup(FMLClientSetupEvent event) {
			ModmakersheadBlock.registerRenderLayer();
		}

		@SubscribeEvent
		public static void blockColorLoad(ColorHandlerEvent.Block event) {
			TungstenOreBlock.blockColorLoad(event);
		}

		@SubscribeEvent
		public static void itemColorLoad(ColorHandlerEvent.Item event) {
			TungstenOreBlock.itemColorLoad(event);
		}
	}
}
