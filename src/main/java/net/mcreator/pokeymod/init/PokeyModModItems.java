
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.pokeymod.init;

import net.mcreator.pokeymod.item.ZeezarPortalIgniterItem;
import net.mcreator.pokeymod.item.ZeezarItem;
import net.mcreator.pokeymod.item.Wooden_armorArmorItem;
import net.mcreator.pokeymod.item.Weighted_Renforced_armorArmorItem;
import net.mcreator.pokeymod.item.TungstenenforcedItem;
import net.mcreator.pokeymod.item.TungstenSwordItem;
import net.mcreator.pokeymod.item.TungstenShovelItem;
import net.mcreator.pokeymod.item.TungstenPickaxeItem;
import net.mcreator.pokeymod.item.TungstenIngotItem;
import net.mcreator.pokeymod.item.TungstenHoeItem;
import net.mcreator.pokeymod.item.TungstenAxeItem;
import net.mcreator.pokeymod.item.Renforced_tungstenSwordItem;
import net.mcreator.pokeymod.item.Renforced_tungstenShovelItem;
import net.mcreator.pokeymod.item.Renforced_tungstenPickaxeItem;
import net.mcreator.pokeymod.item.Renforced_tungstenIngotItem;
import net.mcreator.pokeymod.item.Renforced_tungstenHoeItem;
import net.mcreator.pokeymod.item.Renforced_tungstenAxeItem;
import net.mcreator.pokeymod.item.RenforcedArmorItem;
import net.mcreator.pokeymod.item.PotatoArmorItem;
import net.mcreator.pokeymod.item.PlaceholdermusicdiscItem;
import net.mcreator.pokeymod.item.Leafy_armorArmorItem;
import net.mcreator.pokeymod.item.Drip_amorArmorItem;
import net.mcreator.pokeymod.item.Cobble_stone_armorArmorItem;
import net.mcreator.pokeymod.item.BakedpotatoItem;

import java.util.List;
import java.util.ArrayList;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class PokeyModModItems {
	private static final List<Item> REGISTRY = new ArrayList<>();
	public static final Item ZEEZAR = register(new ZeezarItem());
	public static final Item TUNGSTEN_AROMOR_HELMET = register(new TungstenenforcedItem.Helmet());
	public static final Item TUNGSTEN_AROMOR_CHESTPLATE = register(new TungstenenforcedItem.Chestplate());
	public static final Item TUNGSTEN_AROMOR_LEGGINGS = register(new TungstenenforcedItem.Leggings());
	public static final Item TUNGSTEN_AROMOR_BOOTS = register(new TungstenenforcedItem.Boots());
	public static final Item TUNGSTEN_INGOT = register(new TungstenIngotItem());
	public static final Item TUNGSTEN_ORE = register(PokeyModModBlocks.TUNGSTEN_ORE, PokeyModModTabs.TAB_POKEYSMODTAB);
	public static final Item TUNGSTEN_BLOCK = register(PokeyModModBlocks.TUNGSTEN_BLOCK, PokeyModModTabs.TAB_POKEYSMODTAB);
	public static final Item TUNGSTEN_PICKAXE = register(new TungstenPickaxeItem());
	public static final Item TUNGSTEN_AXE = register(new TungstenAxeItem());
	public static final Item TUNGSTEN_SWORD = register(new TungstenSwordItem());
	public static final Item TUNGSTEN_SHOVEL = register(new TungstenShovelItem());
	public static final Item TUNGSTEN_HOE = register(new TungstenHoeItem());
	public static final Item RENFORCED_TUNGSTEN_INGOT = register(new Renforced_tungstenIngotItem());
	public static final Item RENFORCED_TUNGSTEN_BLOCK = register(PokeyModModBlocks.RENFORCED_TUNGSTEN_BLOCK, PokeyModModTabs.TAB_POKEYSMODTAB);
	public static final Item RENFORCED_ARMOR_HELMET = register(new RenforcedArmorItem.Helmet());
	public static final Item RENFORCED_ARMOR_CHESTPLATE = register(new RenforcedArmorItem.Chestplate());
	public static final Item RENFORCED_ARMOR_LEGGINGS = register(new RenforcedArmorItem.Leggings());
	public static final Item RENFORCED_ARMOR_BOOTS = register(new RenforcedArmorItem.Boots());
	public static final Item RENFORCED_TUNGSTEN_PICKAXE = register(new Renforced_tungstenPickaxeItem());
	public static final Item RENFORCED_TUNGSTEN_AXE = register(new Renforced_tungstenAxeItem());
	public static final Item RENFORCED_TUNGSTEN_SWORD = register(new Renforced_tungstenSwordItem());
	public static final Item RENFORCED_TUNGSTEN_SHOVEL = register(new Renforced_tungstenShovelItem());
	public static final Item RENFORCED_TUNGSTEN_HOE = register(new Renforced_tungstenHoeItem());
	public static final Item MODMAKERSHEAD = register(PokeyModModBlocks.MODMAKERSHEAD, PokeyModModTabs.TAB_POKEYSMODTAB);
	public static final Item WEIGHTED_RENFORCED_ARMOR_ARMOR_HELMET = register(new Weighted_Renforced_armorArmorItem.Helmet());
	public static final Item WEIGHTED_RENFORCED_ARMOR_ARMOR_CHESTPLATE = register(new Weighted_Renforced_armorArmorItem.Chestplate());
	public static final Item WEIGHTED_RENFORCED_ARMOR_ARMOR_LEGGINGS = register(new Weighted_Renforced_armorArmorItem.Leggings());
	public static final Item WEIGHTED_RENFORCED_ARMOR_ARMOR_BOOTS = register(new Weighted_Renforced_armorArmorItem.Boots());
	public static final Item ZEEZAR_PORTAL_IGNITER = register(new ZeezarPortalIgniterItem());
	public static final Item PLACEHOLDERMUSICDISC = register(new PlaceholdermusicdiscItem());
	public static final Item LEAFY_ARMOR_ARMOR_HELMET = register(new Leafy_armorArmorItem.Helmet());
	public static final Item LEAFY_ARMOR_ARMOR_CHESTPLATE = register(new Leafy_armorArmorItem.Chestplate());
	public static final Item LEAFY_ARMOR_ARMOR_LEGGINGS = register(new Leafy_armorArmorItem.Leggings());
	public static final Item LEAFY_ARMOR_ARMOR_BOOTS = register(new Leafy_armorArmorItem.Boots());
	public static final Item WOODEN_ARMOR_ARMOR_HELMET = register(new Wooden_armorArmorItem.Helmet());
	public static final Item WOODEN_ARMOR_ARMOR_CHESTPLATE = register(new Wooden_armorArmorItem.Chestplate());
	public static final Item WOODEN_ARMOR_ARMOR_LEGGINGS = register(new Wooden_armorArmorItem.Leggings());
	public static final Item WOODEN_ARMOR_ARMOR_BOOTS = register(new Wooden_armorArmorItem.Boots());
	public static final Item DRIP_AMOR_ARMOR_HELMET = register(new Drip_amorArmorItem.Helmet());
	public static final Item DRIP_AMOR_ARMOR_CHESTPLATE = register(new Drip_amorArmorItem.Chestplate());
	public static final Item DRIP_AMOR_ARMOR_LEGGINGS = register(new Drip_amorArmorItem.Leggings());
	public static final Item DRIP_AMOR_ARMOR_BOOTS = register(new Drip_amorArmorItem.Boots());
	public static final Item COBBLE_STONE_ARMOR_ARMOR_HELMET = register(new Cobble_stone_armorArmorItem.Helmet());
	public static final Item COBBLE_STONE_ARMOR_ARMOR_CHESTPLATE = register(new Cobble_stone_armorArmorItem.Chestplate());
	public static final Item COBBLE_STONE_ARMOR_ARMOR_LEGGINGS = register(new Cobble_stone_armorArmorItem.Leggings());
	public static final Item COBBLE_STONE_ARMOR_ARMOR_BOOTS = register(new Cobble_stone_armorArmorItem.Boots());
	public static final Item POTATO_ARMOR_HELMET = register(new PotatoArmorItem.Helmet());
	public static final Item POTATO_ARMOR_CHESTPLATE = register(new PotatoArmorItem.Chestplate());
	public static final Item POTATO_ARMOR_LEGGINGS = register(new PotatoArmorItem.Leggings());
	public static final Item POTATO_ARMOR_BOOTS = register(new PotatoArmorItem.Boots());
	public static final Item BAKEDPOTATO_HELMET = register(new BakedpotatoItem.Helmet());
	public static final Item BAKEDPOTATO_CHESTPLATE = register(new BakedpotatoItem.Chestplate());
	public static final Item BAKEDPOTATO_LEGGINGS = register(new BakedpotatoItem.Leggings());
	public static final Item BAKEDPOTATO_BOOTS = register(new BakedpotatoItem.Boots());

	private static Item register(Item item) {
		REGISTRY.add(item);
		return item;
	}

	private static Item register(Block block, CreativeModeTab tab) {
		return register(new BlockItem(block, new Item.Properties().tab(tab)).setRegistryName(block.getRegistryName()));
	}

	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event) {
		event.getRegistry().registerAll(REGISTRY.toArray(new Item[0]));
	}
}
