
package net.mcreator.pokeymod.block;

import net.minecraft.world.level.material.Material;

import net.mcreator.pokeymod.init.PokeyModModBlocks;

import java.util.List;
import java.util.Collections;

public class TungstenOreBlock extends Block {
	public TungstenOreBlock() {
		super(BlockBehaviour.Properties.of(Material.STONE).sound(SoundType.STONE).strength(13.5f, 16.654822761921903f).lightLevel(s -> 2)
				.requiresCorrectToolForDrops().speedFactor(1.5f).jumpFactor(0.25f));
		setRegistryName("tungsten_ore");
	}

	@Override
	public int getLightBlock(BlockState state, BlockGetter worldIn, BlockPos pos) {
		return 14;
	}

	@Override
	public boolean canHarvestBlock(BlockState state, BlockGetter world, BlockPos pos, Player player) {
		if (player.getInventory().getSelected().getItem() instanceof TieredItem tieredItem)
			return tieredItem.getTier().getLevel() >= 9;
		return false;
	}

	@Override
	public List<ItemStack> getDrops(BlockState state, LootContext.Builder builder) {
		List<ItemStack> dropsOriginal = super.getDrops(state, builder);
		if (!dropsOriginal.isEmpty())
			return dropsOriginal;
		return Collections.singletonList(new ItemStack(this, 1));
	}

	@OnlyIn(Dist.CLIENT)
	public static void blockColorLoad(ColorHandlerEvent.Block event) {
		event.getBlockColors().register((bs, world, pos, index) -> {
			return world != null && pos != null ? BiomeColors.getAverageWaterColor(world, pos) : -1;
		}, PokeyModModBlocks.TUNGSTEN_ORE);
	}

	@OnlyIn(Dist.CLIENT)
	public static void itemColorLoad(ColorHandlerEvent.Item event) {
		event.getItemColors().register((stack, index) -> {
			return 3694022;
		}, PokeyModModBlocks.TUNGSTEN_ORE);
	}
}
